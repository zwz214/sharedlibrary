//vars/qavResult.groovy
def info(message) {
    echo "INFO: ${message as String}"
}

def warning(message) {
    echo "WARNING: ${message as String}"
}