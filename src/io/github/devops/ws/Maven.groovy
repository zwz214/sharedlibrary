package io.github.devops.ws;

def Build() {
    sh script: 'mvn clean package', label: 'Maven build'
}

def Fake() {
    bat 'echo do nothing but demonstration'
}
